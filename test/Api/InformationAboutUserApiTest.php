<?php
/**
 * InformationAboutUserApiTest
 * PHP version 5
 *
 * @category Class
 * @package  AllegroApiSDK
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Allegro REST API
 *
 * https://developer.allegro.pl/about
 *
 * The version of the OpenAPI document: latest
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.2.3-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace AllegroApiSDK;

use \AllegroApiSDK\Configuration;
use \AllegroApiSDK\ApiException;
use \AllegroApiSDK\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * InformationAboutUserApiTest Class Doc Comment
 *
 * @category Class
 * @package  AllegroApiSDK
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class InformationAboutUserApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for addAdditionalEmailUsingPOST
     *
     * Add a new additional email address to user's account.
     *
     */
    public function testAddAdditionalEmailUsingPOST()
    {
    }

    /**
     * Test case for deleteAdditionalEmailUsingDELETE
     *
     * Delete an additional email address.
     *
     */
    public function testDeleteAdditionalEmailUsingDELETE()
    {
    }

    /**
     * Test case for getAdditionalEmailUsingGET
     *
     * Get information about a particular additional email.
     *
     */
    public function testGetAdditionalEmailUsingGET()
    {
    }

    /**
     * Test case for getListOfAdditionalEmailsUsingGET
     *
     * Get user's additional emails.
     *
     */
    public function testGetListOfAdditionalEmailsUsingGET()
    {
    }

    /**
     * Test case for getUserRatingsUsingGET
     *
     * Get the user's ratings.
     *
     */
    public function testGetUserRatingsUsingGET()
    {
    }

    /**
     * Test case for meGET
     *
     * Get basic information about user.
     *
     */
    public function testMeGET()
    {
    }
}
