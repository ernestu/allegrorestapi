<?php
/**
 * ProductsApiTest
 * PHP version 5
 *
 * @category Class
 * @package  AllegroApiSDK
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Allegro REST API
 *
 * https://developer.allegro.pl/about
 *
 * The version of the OpenAPI document: latest
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.2.3-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace AllegroApiSDK;

use \AllegroApiSDK\Configuration;
use \AllegroApiSDK\ApiException;
use \AllegroApiSDK\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * ProductsApiTest Class Doc Comment
 *
 * @category Class
 * @package  AllegroApiSDK
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class ProductsApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for getFlatProductParametersUsingGET
     *
     * Get product parameters available in given category.
     *
     */
    public function testGetFlatProductParametersUsingGET()
    {
    }

    /**
     * Test case for getSaleProduct
     *
     * Get all data of the particular product.
     *
     */
    public function testGetSaleProduct()
    {
    }

    /**
     * Test case for getSaleProducts
     *
     * Get search products results.
     *
     */
    public function testGetSaleProducts()
    {
    }

    /**
     * Test case for proposeSaleProduct
     *
     * Propose a product.
     *
     */
    public function testProposeSaleProduct()
    {
    }
}
