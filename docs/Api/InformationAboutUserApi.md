# AllegroApiSDK\InformationAboutUserApi

All URIs are relative to *https://api.allegro.pl*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addAdditionalEmailUsingPOST**](InformationAboutUserApi.md#addAdditionalEmailUsingPOST) | **POST** /account/additional-emails | Add a new additional email address to user&#39;s account
[**deleteAdditionalEmailUsingDELETE**](InformationAboutUserApi.md#deleteAdditionalEmailUsingDELETE) | **DELETE** /account/additional-emails/{emailId} | Delete an additional email address
[**getAdditionalEmailUsingGET**](InformationAboutUserApi.md#getAdditionalEmailUsingGET) | **GET** /account/additional-emails/{emailId} | Get information about a particular additional email
[**getListOfAdditionalEmailsUsingGET**](InformationAboutUserApi.md#getListOfAdditionalEmailsUsingGET) | **GET** /account/additional-emails | Get user&#39;s additional emails
[**getUserRatingsUsingGET**](InformationAboutUserApi.md#getUserRatingsUsingGET) | **GET** /sale/user-ratings | Get the user&#39;s ratings
[**meGET**](InformationAboutUserApi.md#meGET) | **GET** /me | Get basic information about user



## addAdditionalEmailUsingPOST

> \AllegroApiSDK\Model\AdditionalEmail addAdditionalEmailUsingPOST($additional_email_request)

Add a new additional email address to user's account

Use this resource to add a new additional email address to account.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: bearer-token-for-user
$config = AllegroApiSDK\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AllegroApiSDK\Api\InformationAboutUserApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$additional_email_request = new \AllegroApiSDK\Model\AdditionalEmailRequest(); // \AllegroApiSDK\Model\AdditionalEmailRequest | request

try {
    $result = $apiInstance->addAdditionalEmailUsingPOST($additional_email_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformationAboutUserApi->addAdditionalEmailUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **additional_email_request** | [**\AllegroApiSDK\Model\AdditionalEmailRequest**](../Model/AdditionalEmailRequest.md)| request |

### Return type

[**\AllegroApiSDK\Model\AdditionalEmail**](../Model/AdditionalEmail.md)

### Authorization

[bearer-token-for-user](../../README.md#bearer-token-for-user)

### HTTP request headers

- **Content-Type**: application/vnd.allegro.public.v1+json
- **Accept**: application/vnd.allegro.public.v1+json, */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteAdditionalEmailUsingDELETE

> deleteAdditionalEmailUsingDELETE($email_id)

Delete an additional email address

Use this resource to delete one of additional emails.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: bearer-token-for-user
$config = AllegroApiSDK\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AllegroApiSDK\Api\InformationAboutUserApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$email_id = 'email_id_example'; // string | Id of the additional email to be deleted.

try {
    $apiInstance->deleteAdditionalEmailUsingDELETE($email_id);
} catch (Exception $e) {
    echo 'Exception when calling InformationAboutUserApi->deleteAdditionalEmailUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email_id** | **string**| Id of the additional email to be deleted. |

### Return type

void (empty response body)

### Authorization

[bearer-token-for-user](../../README.md#bearer-token-for-user)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/vnd.allegro.public.v1+json, */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getAdditionalEmailUsingGET

> \AllegroApiSDK\Model\AdditionalEmail getAdditionalEmailUsingGET($email_id)

Get information about a particular additional email

Use this resource to retrieve a single additional email.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: bearer-token-for-user
$config = AllegroApiSDK\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AllegroApiSDK\Api\InformationAboutUserApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$email_id = 'email_id_example'; // string | Id of the additional email.

try {
    $result = $apiInstance->getAdditionalEmailUsingGET($email_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformationAboutUserApi->getAdditionalEmailUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email_id** | **string**| Id of the additional email. |

### Return type

[**\AllegroApiSDK\Model\AdditionalEmail**](../Model/AdditionalEmail.md)

### Authorization

[bearer-token-for-user](../../README.md#bearer-token-for-user)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/vnd.allegro.public.v1+json, */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getListOfAdditionalEmailsUsingGET

> \AllegroApiSDK\Model\AdditionalEmailsResponse getListOfAdditionalEmailsUsingGET()

Get user's additional emails

Use this resource to get a list of all additional email addresses assigned to account.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: bearer-token-for-user
$config = AllegroApiSDK\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AllegroApiSDK\Api\InformationAboutUserApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getListOfAdditionalEmailsUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformationAboutUserApi->getListOfAdditionalEmailsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\AllegroApiSDK\Model\AdditionalEmailsResponse**](../Model/AdditionalEmailsResponse.md)

### Authorization

[bearer-token-for-user](../../README.md#bearer-token-for-user)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/vnd.allegro.public.v1+json, */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getUserRatingsUsingGET

> \AllegroApiSDK\Model\UserRatingListResponse getUserRatingsUsingGET($user_id, $recommended, $offset, $limit)

Get the user's ratings

Use this resource to receive your sales score. <a href=\"../../news/2017-10-09-news_informacje_o_ocenach/\" target=\"_blank\">Read more</a>.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: bearer-token-for-user
$config = AllegroApiSDK\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AllegroApiSDK\Api\InformationAboutUserApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user_id = 'user_id_example'; // string | Filter by user id, you are allowed to get your ratings only.
$recommended = 'recommended_example'; // string | Filter by recommended.
$offset = 0; // int | The offset of elements in the response.
$limit = 20; // int | The limit of elements in the response.

try {
    $result = $apiInstance->getUserRatingsUsingGET($user_id, $recommended, $offset, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformationAboutUserApi->getUserRatingsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **string**| Filter by user id, you are allowed to get your ratings only. |
 **recommended** | **string**| Filter by recommended. | [optional]
 **offset** | **int**| The offset of elements in the response. | [optional] [default to 0]
 **limit** | **int**| The limit of elements in the response. | [optional] [default to 20]

### Return type

[**\AllegroApiSDK\Model\UserRatingListResponse**](../Model/UserRatingListResponse.md)

### Authorization

[bearer-token-for-user](../../README.md#bearer-token-for-user)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/vnd.allegro.public.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## meGET

> \AllegroApiSDK\Model\MeResponse meGET()

Get basic information about user

Use this resource when you need basic information about authenticated user.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: bearer-token-for-user
$config = AllegroApiSDK\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AllegroApiSDK\Api\InformationAboutUserApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->meGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InformationAboutUserApi->meGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\AllegroApiSDK\Model\MeResponse**](../Model/MeResponse.md)

### Authorization

[bearer-token-for-user](../../README.md#bearer-token-for-user)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/vnd.allegro.public.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

