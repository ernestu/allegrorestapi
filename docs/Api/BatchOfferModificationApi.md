# AllegroApiSDK\BatchOfferModificationApi

All URIs are relative to *https://api.allegro.pl*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getGeneralReportUsingGET**](BatchOfferModificationApi.md#getGeneralReportUsingGET) | **GET** /sale/offer-modification-commands/{commandId} | Modification command summary
[**getPriceModificationCommandStatusUsingGET**](BatchOfferModificationApi.md#getPriceModificationCommandStatusUsingGET) | **GET** /sale/offer-price-change-commands/{commandId} | Change price command summary
[**getPriceModificationCommandTasksStatusesUsingGET**](BatchOfferModificationApi.md#getPriceModificationCommandTasksStatusesUsingGET) | **GET** /sale/offer-price-change-commands/{commandId}/tasks | Change price command detailed report
[**getQuantityModificationCommandStatusUsingGET**](BatchOfferModificationApi.md#getQuantityModificationCommandStatusUsingGET) | **GET** /sale/offer-quantity-change-commands/{commandId} | Change quantity command summary
[**getQuantityModificationCommandTasksStatusesUsingGET**](BatchOfferModificationApi.md#getQuantityModificationCommandTasksStatusesUsingGET) | **GET** /sale/offer-quantity-change-commands/{commandId}/tasks | Change quantity command detailed report
[**getTasksUsingGET**](BatchOfferModificationApi.md#getTasksUsingGET) | **GET** /sale/offer-modification-commands/{commandId}/tasks | Modification command detailed report
[**modificationCommandUsingPUT**](BatchOfferModificationApi.md#modificationCommandUsingPUT) | **PUT** /sale/offer-modification-commands/{commandId} | Batch offer modification
[**priceModificationCommandUsingPUT**](BatchOfferModificationApi.md#priceModificationCommandUsingPUT) | **PUT** /sale/offer-price-change-commands/{commandId} | Batch offer price modification
[**quantityModificationCommandUsingPUT**](BatchOfferModificationApi.md#quantityModificationCommandUsingPUT) | **PUT** /sale/offer-quantity-change-commands/{commandId} | Batch offer quantity modification



## getGeneralReportUsingGET

> \AllegroApiSDK\Model\GeneralReport getGeneralReportUsingGET($command_id)

Modification command summary

Use this resource to find out how many offers were edited within one {commandId}. You will receive a summary with a number of successfully edited offers. <a href=\"../../news/2018-04-19-news_grupowa_edycja_ofert_update/#2\" target=\"_blank\">Read more</a>.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: bearer-token-for-user
$config = AllegroApiSDK\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AllegroApiSDK\Api\BatchOfferModificationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$command_id = 'command_id_example'; // string | Command identifier.

try {
    $result = $apiInstance->getGeneralReportUsingGET($command_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BatchOfferModificationApi->getGeneralReportUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **command_id** | **string**| Command identifier. |

### Return type

[**\AllegroApiSDK\Model\GeneralReport**](../Model/GeneralReport.md)

### Authorization

[bearer-token-for-user](../../README.md#bearer-token-for-user)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/vnd.allegro.public.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPriceModificationCommandStatusUsingGET

> \AllegroApiSDK\Model\GeneralReport getPriceModificationCommandStatusUsingGET($command_id)

Change price command summary

Returns status and summary of particular command execution.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: bearer-token-for-user
$config = AllegroApiSDK\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AllegroApiSDK\Api\BatchOfferModificationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$command_id = 'command_id_example'; // string | Command identifier.

try {
    $result = $apiInstance->getPriceModificationCommandStatusUsingGET($command_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BatchOfferModificationApi->getPriceModificationCommandStatusUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **command_id** | **string**| Command identifier. |

### Return type

[**\AllegroApiSDK\Model\GeneralReport**](../Model/GeneralReport.md)

### Authorization

[bearer-token-for-user](../../README.md#bearer-token-for-user)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/vnd.allegro.public.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPriceModificationCommandTasksStatusesUsingGET

> \AllegroApiSDK\Model\TaskReport getPriceModificationCommandTasksStatusesUsingGET($command_id, $limit, $offset)

Change price command detailed report

Defaults: limit = 100, offset = 0.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: bearer-token-for-user
$config = AllegroApiSDK\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AllegroApiSDK\Api\BatchOfferModificationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$command_id = 'command_id_example'; // string | Command identifier.
$limit = 56; // int | The limit of elements in the response.
$offset = 56; // int | The offset of elements in the response.

try {
    $result = $apiInstance->getPriceModificationCommandTasksStatusesUsingGET($command_id, $limit, $offset);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BatchOfferModificationApi->getPriceModificationCommandTasksStatusesUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **command_id** | **string**| Command identifier. |
 **limit** | **int**| The limit of elements in the response. | [optional]
 **offset** | **int**| The offset of elements in the response. | [optional]

### Return type

[**\AllegroApiSDK\Model\TaskReport**](../Model/TaskReport.md)

### Authorization

[bearer-token-for-user](../../README.md#bearer-token-for-user)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/vnd.allegro.public.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getQuantityModificationCommandStatusUsingGET

> \AllegroApiSDK\Model\GeneralReport getQuantityModificationCommandStatusUsingGET($command_id)

Change quantity command summary

Returns status and summary of the command.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: bearer-token-for-user
$config = AllegroApiSDK\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AllegroApiSDK\Api\BatchOfferModificationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$command_id = 'command_id_example'; // string | Command identifier.

try {
    $result = $apiInstance->getQuantityModificationCommandStatusUsingGET($command_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BatchOfferModificationApi->getQuantityModificationCommandStatusUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **command_id** | **string**| Command identifier. |

### Return type

[**\AllegroApiSDK\Model\GeneralReport**](../Model/GeneralReport.md)

### Authorization

[bearer-token-for-user](../../README.md#bearer-token-for-user)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/vnd.allegro.public.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getQuantityModificationCommandTasksStatusesUsingGET

> \AllegroApiSDK\Model\TaskReport getQuantityModificationCommandTasksStatusesUsingGET($command_id, $limit, $offset)

Change quantity command detailed report

Defaults: limit = 100, offset = 0.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: bearer-token-for-user
$config = AllegroApiSDK\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AllegroApiSDK\Api\BatchOfferModificationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$command_id = 'command_id_example'; // string | Command identifier.
$limit = 56; // int | The limit of elements in the response.
$offset = 56; // int | The offset of elements in the response.

try {
    $result = $apiInstance->getQuantityModificationCommandTasksStatusesUsingGET($command_id, $limit, $offset);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BatchOfferModificationApi->getQuantityModificationCommandTasksStatusesUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **command_id** | **string**| Command identifier. |
 **limit** | **int**| The limit of elements in the response. | [optional]
 **offset** | **int**| The offset of elements in the response. | [optional]

### Return type

[**\AllegroApiSDK\Model\TaskReport**](../Model/TaskReport.md)

### Authorization

[bearer-token-for-user](../../README.md#bearer-token-for-user)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/vnd.allegro.public.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getTasksUsingGET

> \AllegroApiSDK\Model\TaskReport getTasksUsingGET($command_id, $limit, $offset)

Modification command detailed report

Use this resource to retrieve a detailed summary of changes introduced within one {commandId} (defaults: limit = 100, offset = 0). <a href=\"../../news/2018-04-19-news_grupowa_edycja_ofert_update/#2\" target=\"_blank\">Read more</a>.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: bearer-token-for-user
$config = AllegroApiSDK\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AllegroApiSDK\Api\BatchOfferModificationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$command_id = 'command_id_example'; // string | Command identifier.
$limit = 56; // int | The limit of elements in the response.
$offset = 56; // int | The offset of elements in the response.

try {
    $result = $apiInstance->getTasksUsingGET($command_id, $limit, $offset);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BatchOfferModificationApi->getTasksUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **command_id** | **string**| Command identifier. |
 **limit** | **int**| The limit of elements in the response. | [optional]
 **offset** | **int**| The offset of elements in the response. | [optional]

### Return type

[**\AllegroApiSDK\Model\TaskReport**](../Model/TaskReport.md)

### Authorization

[bearer-token-for-user](../../README.md#bearer-token-for-user)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/vnd.allegro.public.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## modificationCommandUsingPUT

> \AllegroApiSDK\Model\GeneralReport modificationCommandUsingPUT($command_id, $offer_change_command)

Batch offer modification

Use this resource to modify multiple offers at once. <a href=\"../../news/2018-04-19-news_grupowa_edycja_ofert_update/#1\" target=\"_blank\">Read more</a>.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: bearer-token-for-user
$config = AllegroApiSDK\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AllegroApiSDK\Api\BatchOfferModificationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$command_id = 'command_id_example'; // string | Command identifier.
$offer_change_command = new \AllegroApiSDK\Model\OfferChangeCommand(); // \AllegroApiSDK\Model\OfferChangeCommand | offerChangeCommandDto

try {
    $result = $apiInstance->modificationCommandUsingPUT($command_id, $offer_change_command);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BatchOfferModificationApi->modificationCommandUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **command_id** | **string**| Command identifier. |
 **offer_change_command** | [**\AllegroApiSDK\Model\OfferChangeCommand**](../Model/OfferChangeCommand.md)| offerChangeCommandDto |

### Return type

[**\AllegroApiSDK\Model\GeneralReport**](../Model/GeneralReport.md)

### Authorization

[bearer-token-for-user](../../README.md#bearer-token-for-user)

### HTTP request headers

- **Content-Type**: application/vnd.allegro.public.v1+json
- **Accept**: application/vnd.allegro.public.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## priceModificationCommandUsingPUT

> \AllegroApiSDK\Model\GeneralReport priceModificationCommandUsingPUT($command_id, $offer_price_change_command)

Batch offer price modification

Change price of offers.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: bearer-token-for-user
$config = AllegroApiSDK\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AllegroApiSDK\Api\BatchOfferModificationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$command_id = 'command_id_example'; // string | Command identifier.
$offer_price_change_command = new \AllegroApiSDK\Model\OfferPriceChangeCommand(); // \AllegroApiSDK\Model\OfferPriceChangeCommand | offerPriceChangeCommandDto

try {
    $result = $apiInstance->priceModificationCommandUsingPUT($command_id, $offer_price_change_command);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BatchOfferModificationApi->priceModificationCommandUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **command_id** | **string**| Command identifier. |
 **offer_price_change_command** | [**\AllegroApiSDK\Model\OfferPriceChangeCommand**](../Model/OfferPriceChangeCommand.md)| offerPriceChangeCommandDto |

### Return type

[**\AllegroApiSDK\Model\GeneralReport**](../Model/GeneralReport.md)

### Authorization

[bearer-token-for-user](../../README.md#bearer-token-for-user)

### HTTP request headers

- **Content-Type**: application/vnd.allegro.public.v1+json
- **Accept**: application/vnd.allegro.public.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## quantityModificationCommandUsingPUT

> \AllegroApiSDK\Model\GeneralReport quantityModificationCommandUsingPUT($command_id, $offer_quantity_change_command)

Batch offer quantity modification

Change quantity of multiple offers.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: bearer-token-for-user
$config = AllegroApiSDK\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new AllegroApiSDK\Api\BatchOfferModificationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$command_id = 'command_id_example'; // string | Command identifier.
$offer_quantity_change_command = new \AllegroApiSDK\Model\OfferQuantityChangeCommand(); // \AllegroApiSDK\Model\OfferQuantityChangeCommand | offerQuantityChangeCommandDto

try {
    $result = $apiInstance->quantityModificationCommandUsingPUT($command_id, $offer_quantity_change_command);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BatchOfferModificationApi->quantityModificationCommandUsingPUT: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **command_id** | **string**| Command identifier. |
 **offer_quantity_change_command** | [**\AllegroApiSDK\Model\OfferQuantityChangeCommand**](../Model/OfferQuantityChangeCommand.md)| offerQuantityChangeCommandDto |

### Return type

[**\AllegroApiSDK\Model\GeneralReport**](../Model/GeneralReport.md)

### Authorization

[bearer-token-for-user](../../README.md#bearer-token-for-user)

### HTTP request headers

- **Content-Type**: application/vnd.allegro.public.v1+json
- **Accept**: application/vnd.allegro.public.v1+json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

