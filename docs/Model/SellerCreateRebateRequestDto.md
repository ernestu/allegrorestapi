# # SellerCreateRebateRequestDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**benefits** | [**\AllegroApiSDK\Model\Benefit[]**](Benefit.md) | What kind of rebate will be given | 
**offer_criteria** | [**\AllegroApiSDK\Model\SellerRebateOfferCriterion[]**](SellerRebateOfferCriterion.md) | What offers will be included | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


