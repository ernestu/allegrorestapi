# # OfferListingDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | The offer ID. | [optional] 
**name** | **string** | The title of the offer. | [optional] 
**category** | [**\AllegroApiSDK\Model\OfferCategory**](OfferCategory.md) |  | [optional] 
**primary_image** | [**\AllegroApiSDK\Model\OfferListingDtoImage**](OfferListingDtoImage.md) |  | [optional] 
**selling_mode** | [**\AllegroApiSDK\Model\SellingMode**](SellingMode.md) |  | [optional] 
**sale_info** | [**\AllegroApiSDK\Model\OfferListingDtoV1SaleInfo**](OfferListingDtoV1SaleInfo.md) |  | [optional] 
**stock** | [**\AllegroApiSDK\Model\OfferListingDtoV1Stock**](OfferListingDtoV1Stock.md) |  | [optional] 
**stats** | [**\AllegroApiSDK\Model\OfferListingDtoV1Stats**](OfferListingDtoV1Stats.md) |  | [optional] 
**publication** | [**\AllegroApiSDK\Model\OfferListingDtoV1Publication**](OfferListingDtoV1Publication.md) |  | [optional] 
**after_sales_services** | [**\AllegroApiSDK\Model\AfterSalesServices**](AfterSalesServices.md) |  | [optional] 
**additional_services** | [**\AllegroApiSDK\Model\OfferAdditionalServices**](OfferAdditionalServices.md) |  | [optional] 
**external** | [**\AllegroApiSDK\Model\ExternalId**](ExternalId.md) |  | [optional] 
**delivery** | [**\AllegroApiSDK\Model\OfferListingDtoV1Delivery**](OfferListingDtoV1Delivery.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


