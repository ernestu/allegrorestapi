# # MessageRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **string** |  | 
**attachment** | [**\AllegroApiSDK\Model\DisputeAttachmentId**](DisputeAttachmentId.md) |  | 
**type** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


