# # BadgeApplicationRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign** | [**\AllegroApiSDK\Model\BadgeApplicationCampaign**](BadgeApplicationCampaign.md) |  | 
**offer** | [**\AllegroApiSDK\Model\BadgeApplicationOffer**](BadgeApplicationOffer.md) |  | 
**prices** | [**\AllegroApiSDK\Model\BadgeApplicationPrices**](BadgeApplicationPrices.md) |  | [optional] 
**purchase_constraints** | [**\AllegroApiSDK\Model\BadgeApplicationPurchaseConstraints**](BadgeApplicationPurchaseConstraints.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


