# # ListingResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**\AllegroApiSDK\Model\ListingResponseOffers**](ListingResponseOffers.md) |  | [optional] 
**categories** | [**\AllegroApiSDK\Model\ListingResponseCategories**](ListingResponseCategories.md) |  | [optional] 
**filters** | [**\AllegroApiSDK\Model\ListingResponseFilters[]**](ListingResponseFilters.md) | An array of filters with counters available for given search. This can be used to refine the search results. | [optional] 
**search_meta** | [**\AllegroApiSDK\Model\ListingResponseSearchMeta**](ListingResponseSearchMeta.md) |  | [optional] 
**sort** | [**\AllegroApiSDK\Model\ListingResponseSort[]**](ListingResponseSort.md) | Available sorting options. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


