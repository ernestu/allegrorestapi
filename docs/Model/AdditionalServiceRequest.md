# # AdditionalServiceRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**definition** | [**\AllegroApiSDK\Model\AdditionalServiceDefinitionRequest**](AdditionalServiceDefinitionRequest.md) |  | 
**description** | **string** |  | 
**configurations** | [**\AllegroApiSDK\Model\Configuration[]**](Configuration.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


