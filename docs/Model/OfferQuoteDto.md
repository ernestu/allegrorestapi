# # OfferQuoteDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **bool** |  | [optional] 
**fee** | [**\AllegroApiSDK\Model\Fee**](Fee.md) |  | [optional] 
**name** | **string** |  | [optional] 
**next_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**offer** | [**\AllegroApiSDK\Model\Offer**](Offer.md) |  | [optional] 
**type** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


