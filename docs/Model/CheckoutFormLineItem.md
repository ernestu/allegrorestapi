# # CheckoutFormLineItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Line item identifier | 
**offer** | [**\AllegroApiSDK\Model\OfferReference**](OfferReference.md) |  | 
**quantity** | **float** | quantity | 
**original_price** | [**\AllegroApiSDK\Model\Price**](Price.md) |  | 
**price** | [**\AllegroApiSDK\Model\Price**](Price.md) |  | 
**selected_additional_services** | [**\AllegroApiSDK\Model\CheckoutFormAdditionalService[]**](CheckoutFormAdditionalService.md) |  | [optional] 
**bought_at** | [**\DateTime**](\DateTime.md) | ISO date when offer was bought | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


