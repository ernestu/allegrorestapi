# # ProductParameter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | 
**range_value** | [**\AllegroApiSDK\Model\ParameterRangeValue**](ParameterRangeValue.md) |  | [optional] 
**values** | **string[]** |  | [optional] 
**values_ids** | **string[]** |  | [optional] 
**values_labels** | **string[]** |  | [optional] 
**unit** | **string** |  | [optional] 
**options** | [**\AllegroApiSDK\Model\ProductParameterOptions[]**](ProductParameterOptions.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


