# # BundleBenefitSpecification

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional] [default to 'ORDER_FIXED_DISCOUNT']
**value** | [**\AllegroApiSDK\Model\Price**](Price.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


