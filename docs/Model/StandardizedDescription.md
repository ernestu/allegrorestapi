# # StandardizedDescription

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sections** | [**\AllegroApiSDK\Model\DescriptionSection[]**](DescriptionSection.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


