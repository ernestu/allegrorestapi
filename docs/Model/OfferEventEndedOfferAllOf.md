# # OfferEventEndedOfferAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**publication** | [**\AllegroApiSDK\Model\OfferEventEndedOfferAllOfPublication**](OfferEventEndedOfferAllOfPublication.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


