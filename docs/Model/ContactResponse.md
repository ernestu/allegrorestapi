# # ContactResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**emails** | [**\AllegroApiSDK\Model\EmailResponse[]**](EmailResponse.md) |  | [optional] 
**phones** | [**\AllegroApiSDK\Model\PhonesResponse[]**](PhonesResponse.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


