# # ContributionOperation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional] [default to 'CONTRIBUTION']
**payment** | [**\AllegroApiSDK\Model\OperationPayment**](OperationPayment.md) |  | 
**participant** | [**\AllegroApiSDK\Model\BuyerParticipant**](BuyerParticipant.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


