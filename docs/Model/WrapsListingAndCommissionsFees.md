# # WrapsListingAndCommissionsFees

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commissions** | [**\AllegroApiSDK\Model\DescribesSuccessCommissionFee[]**](DescribesSuccessCommissionFee.md) |  | 
**quotes** | [**\AllegroApiSDK\Model\DescribesListingFee[]**](DescribesListingFee.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


