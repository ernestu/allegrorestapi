# # CompatibleProductDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Identifier of the compatible product. | [optional] 
**text** | **string** | Textual representation of the compatible product. | [optional] 
**group** | [**\AllegroApiSDK\Model\CompatibleProductDtoGroup**](CompatibleProductDtoGroup.md) |  | [optional] 
**attributes** | [**\AllegroApiSDK\Model\CompatibleProductDtoAttributes[]**](CompatibleProductDtoAttributes.md) | List of compatible products attributes. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


