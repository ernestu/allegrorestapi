# # PublicTableImageDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**captions** | [**\AllegroApiSDK\Model\Caption[]**](Caption.md) |  | 
**url** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


