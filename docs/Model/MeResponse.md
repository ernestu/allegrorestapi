# # MeResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | User Id. | [optional] 
**login** | **string** | User login. | [optional] 
**first_name** | **string** | User&#39;s first name. | [optional] 
**last_name** | **string** | User&#39;s last name. | [optional] 
**company** | [**\AllegroApiSDK\Model\Company**](Company.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


