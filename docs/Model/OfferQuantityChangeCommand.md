# # OfferQuantityChangeCommand

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**modification** | [**\AllegroApiSDK\Model\QuantityModification**](QuantityModification.md) |  | [optional] 
**offer_criteria** | [**\AllegroApiSDK\Model\OfferCriterium[]**](OfferCriterium.md) | List of offer criteria | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


