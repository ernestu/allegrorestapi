# # UserRatingSummaryResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**average_rates** | [**\AllegroApiSDK\Model\AverageRates**](AverageRates.md) |  | [optional] 
**not_recommended** | [**\AllegroApiSDK\Model\UserRatingSummaryResponseNotRecommended**](UserRatingSummaryResponseNotRecommended.md) |  | 
**recommended** | [**\AllegroApiSDK\Model\UserRatingSummaryResponseRecommended**](UserRatingSummaryResponseRecommended.md) |  | 
**recommended_percentage** | **string** | Percentage of unique buyers recommending the seller. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


