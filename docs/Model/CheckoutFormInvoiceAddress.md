# # CheckoutFormInvoiceAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **string** | Street name | 
**city** | **string** | City name | 
**zip_code** | **string** | Postal code | 
**country_code** | **string** | Country code | 
**company** | [**\AllegroApiSDK\Model\CheckoutFormInvoiceAddressCompany**](CheckoutFormInvoiceAddressCompany.md) |  | [optional] 
**natural_person** | [**\AllegroApiSDK\Model\CheckoutFormInvoiceAddressNaturalPerson**](CheckoutFormInvoiceAddressNaturalPerson.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


