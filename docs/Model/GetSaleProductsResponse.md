# # GetSaleProductsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**products** | [**\AllegroApiSDK\Model\SaleProductResponseDto[]**](SaleProductResponseDto.md) |  | 
**categories** | [**\AllegroApiSDK\Model\SaleProductResponseCategoriesDto[]**](SaleProductResponseCategoriesDto.md) |  | [optional] 
**filters** | [**\AllegroApiSDK\Model\ListingResponseFilters[]**](ListingResponseFilters.md) |  | [optional] 
**next_page** | [**\AllegroApiSDK\Model\GetSaleProductsResponseNextPage**](GetSaleProductsResponseNextPage.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


