# # PublicOfferPreviewRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offer** | [**\AllegroApiSDK\Model\Offer**](Offer.md) |  | [optional] 
**classifieds_packages** | [**\AllegroApiSDK\Model\ClassifiedsPackages**](ClassifiedsPackages.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


