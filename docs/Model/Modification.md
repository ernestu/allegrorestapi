# # Modification

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additional_services_group** | [**\AllegroApiSDK\Model\AdditionalServicesGroup**](AdditionalServicesGroup.md) |  | [optional] 
**delivery** | [**\AllegroApiSDK\Model\ModificationDelivery**](ModificationDelivery.md) |  | [optional] 
**payments** | [**\AllegroApiSDK\Model\ModificationPayments**](ModificationPayments.md) |  | [optional] 
**promotion** | [**\AllegroApiSDK\Model\ModificationPromotion**](ModificationPromotion.md) |  | [optional] 
**size_table** | [**\AllegroApiSDK\Model\ModificationSizeTable**](ModificationSizeTable.md) |  | [optional] 
**publication** | [**\AllegroApiSDK\Model\ModificationPublication**](ModificationPublication.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


