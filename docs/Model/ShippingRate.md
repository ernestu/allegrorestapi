# # ShippingRate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delivery_method** | [**\AllegroApiSDK\Model\ShippingRateDeliveryMethod**](ShippingRateDeliveryMethod.md) |  | 
**max_quantity_per_package** | **int** | Maximum quantity per package for the given delivery method. Minimum value is 1. | 
**first_item_rate** | [**\AllegroApiSDK\Model\ShippingRateFirstItemRate**](ShippingRateFirstItemRate.md) |  | 
**next_item_rate** | [**\AllegroApiSDK\Model\ShippingRateNextItemRate**](ShippingRateNextItemRate.md) |  | 
**shipping_time** | [**\AllegroApiSDK\Model\ShippingRateShippingTime**](ShippingRateShippingTime.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


