# # AfterSalesServices

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**implied_warranty** | [**\AllegroApiSDK\Model\ImpliedWarranty**](ImpliedWarranty.md) |  | [optional] 
**return_policy** | [**\AllegroApiSDK\Model\ReturnPolicy**](ReturnPolicy.md) |  | [optional] 
**warranty** | [**\AllegroApiSDK\Model\Warranty**](Warranty.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


