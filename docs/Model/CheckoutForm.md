# # CheckoutForm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Checkout form id | 
**message_to_seller** | **string** | Message from buyer to seller | [optional] 
**buyer** | [**\AllegroApiSDK\Model\CheckoutFormBuyerReference**](CheckoutFormBuyerReference.md) |  | 
**payment** | [**\AllegroApiSDK\Model\CheckoutFormPaymentReference**](CheckoutFormPaymentReference.md) |  | [optional] 
**status** | [**\AllegroApiSDK\Model\CheckoutFormStatus**](CheckoutFormStatus.md) |  | 
**delivery** | [**\AllegroApiSDK\Model\CheckoutFormDeliveryReference**](CheckoutFormDeliveryReference.md) |  | [optional] 
**invoice** | [**\AllegroApiSDK\Model\CheckoutFormInvoiceInfo**](CheckoutFormInvoiceInfo.md) |  | [optional] 
**line_items** | [**\AllegroApiSDK\Model\CheckoutFormLineItem[]**](CheckoutFormLineItem.md) |  | 
**surcharges** | [**\AllegroApiSDK\Model\CheckoutFormPaymentReference[]**](CheckoutFormPaymentReference.md) |  | 
**discounts** | [**\AllegroApiSDK\Model\CheckoutFormDiscount[]**](CheckoutFormDiscount.md) |  | 
**summary** | [**\AllegroApiSDK\Model\CheckoutFormSummary**](CheckoutFormSummary.md) |  | 
**updated_at** | **string** | Provided in [ISO 8601 format](link: https://en.wikipedia.org/wiki/ISO_8601). | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


