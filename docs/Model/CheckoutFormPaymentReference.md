# # CheckoutFormPaymentReference

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Payment id | 
**type** | [**\AllegroApiSDK\Model\CheckoutFormPaymentType**](CheckoutFormPaymentType.md) |  | 
**provider** | [**\AllegroApiSDK\Model\CheckoutFormPaymentProvider**](CheckoutFormPaymentProvider.md) |  | [optional] 
**finished_at** | [**\DateTime**](\DateTime.md) | Date when the event occurred | [optional] 
**paid_amount** | [**\AllegroApiSDK\Model\Price**](Price.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


