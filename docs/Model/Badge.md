# # Badge

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offer** | [**\AllegroApiSDK\Model\BadgeApplicationOffer**](BadgeApplicationOffer.md) |  | 
**campaign** | [**\AllegroApiSDK\Model\OfferBadgeCampaign**](OfferBadgeCampaign.md) |  | 
**publication** | [**\AllegroApiSDK\Model\BadgePublicationTimePolicy**](BadgePublicationTimePolicy.md) |  | [optional] 
**prices** | [**\AllegroApiSDK\Model\BadgeApplicationPrices**](BadgeApplicationPrices.md) |  | [optional] 
**process** | [**\AllegroApiSDK\Model\BadgeProcess**](BadgeProcess.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


