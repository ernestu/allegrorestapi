# # CommandOutput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**\AllegroApiSDK\Model\ProcessingStatus**](ProcessingStatus.md) |  | [optional] 
**errors** | [**\AllegroApiSDK\Model\Error[]**](Error.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


