# # DisputeMessage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | 
**text** | **string** |  | [optional] 
**attachment** | [**\AllegroApiSDK\Model\DisputeAttachment**](DisputeAttachment.md) |  | [optional] 
**author** | [**\AllegroApiSDK\Model\DisputeMessageAuthor**](DisputeMessageAuthor.md) |  | 
**created_at** | [**\DateTime**](\DateTime.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


