# # Offer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additional_services** | [**\AllegroApiSDK\Model\JustId**](JustId.md) |  | [optional] 
**after_sales_services** | [**\AllegroApiSDK\Model\AfterSalesServices**](AfterSalesServices.md) |  | [optional] 
**attachments** | [**\AllegroApiSDK\Model\Attachment[]**](Attachment.md) | List of offer attachments. You can attach up to 7 files to the offer – one per each attachment type as described in &lt;a href&#x3D;\&quot;/documentation/#operation/createOfferAttachmentUsingPOST\&quot; target&#x3D;\&quot;_blank\&quot;&gt;uploading offer attachments flow&lt;/a&gt;. | [optional] 
**category** | [**\AllegroApiSDK\Model\Category**](Category.md) |  | [optional] 
**compatibility_list** | [**\AllegroApiSDK\Model\CompatibilityList**](CompatibilityList.md) |  | [optional] 
**contact** | [**\AllegroApiSDK\Model\JustId**](JustId.md) |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Creation date: Format (ISO 8601) - yyyy-MM-dd&#39;T&#39;HH:mm:ss.SSSZ. Cannot be modified. | [optional] 
**delivery** | [**\AllegroApiSDK\Model\Delivery**](Delivery.md) |  | [optional] 
**description** | [**\AllegroApiSDK\Model\StandardizedDescription**](StandardizedDescription.md) |  | [optional] 
**ean** | **string** |  | [optional] 
**external** | [**\AllegroApiSDK\Model\ExternalId**](ExternalId.md) |  | [optional] 
**id** | **string** |  | [optional] 
**images** | [**\AllegroApiSDK\Model\ImageUrl[]**](ImageUrl.md) |  | [optional] 
**location** | [**\AllegroApiSDK\Model\Location**](Location.md) |  | [optional] 
**name** | **string** | Name of the offer. Words used in the name field cannot be longer than 30 characters. | 
**parameters** | [**\AllegroApiSDK\Model\Parameter[]**](Parameter.md) |  | [optional] 
**payments** | [**\AllegroApiSDK\Model\Payments**](Payments.md) |  | [optional] 
**product** | [**\AllegroApiSDK\Model\JustId**](JustId.md) |  | [optional] 
**promotion** | [**\AllegroApiSDK\Model\Promotion**](Promotion.md) |  | [optional] 
**publication** | [**\AllegroApiSDK\Model\Publication**](Publication.md) |  | [optional] 
**selling_mode** | [**\AllegroApiSDK\Model\SellingMode**](SellingMode.md) |  | [optional] 
**size_table** | [**\AllegroApiSDK\Model\JustId**](JustId.md) |  | [optional] 
**stock** | [**\AllegroApiSDK\Model\Stock**](Stock.md) |  | [optional] 
**tecdoc_specification** | [**\AllegroApiSDK\Model\TecdocSpecification**](TecdocSpecification.md) |  | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Last update date: Format (ISO 8601) - yyyy-MM-dd&#39;T&#39;HH:mm:ss.SSSZ. Cannot be modified | [optional] 
**validation** | [**\AllegroApiSDK\Model\Validation**](Validation.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


