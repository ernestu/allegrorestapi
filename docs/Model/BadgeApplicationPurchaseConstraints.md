# # BadgeApplicationPurchaseConstraints

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**limit** | [**\AllegroApiSDK\Model\BadgeApplicationPurchaseConstraintsLimit**](BadgeApplicationPurchaseConstraintsLimit.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


