# # Dispute

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Identifier of the dispute | 
**subject** | [**\AllegroApiSDK\Model\Subject**](Subject.md) |  | 
**status** | **string** |  | 
**buyer** | [**\AllegroApiSDK\Model\DisputeUser**](DisputeUser.md) |  | 
**seller** | [**\AllegroApiSDK\Model\DisputeUser**](DisputeUser.md) |  | 
**checkout_form** | [**\AllegroApiSDK\Model\DisputeCheckoutForm**](DisputeCheckoutForm.md) |  | 
**message** | [**\AllegroApiSDK\Model\DisputeFirstMessage**](DisputeFirstMessage.md) |  | 
**messages_count** | **int** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


