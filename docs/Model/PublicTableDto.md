# # PublicTableDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**headers** | [**\AllegroApiSDK\Model\Header[]**](Header.md) |  | 
**id** | **string** |  | [optional] 
**image** | [**\AllegroApiSDK\Model\PublicTableImageDto**](PublicTableImageDto.md) |  | [optional] 
**name** | **string** |  | 
**orientation** | **string** |  | 
**values** | [**\AllegroApiSDK\Model\Cells[]**](Cells.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


