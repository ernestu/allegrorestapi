# # OfferAttachmentRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\AllegroApiSDK\Model\AttachmentType**](AttachmentType.md) |  | [optional] 
**file** | [**\AllegroApiSDK\Model\AttachmentFileRequest**](AttachmentFileRequest.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


