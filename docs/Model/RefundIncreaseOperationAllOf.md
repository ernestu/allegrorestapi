# # RefundIncreaseOperationAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional] [default to 'REFUND_INCREASE']
**payment** | [**\AllegroApiSDK\Model\OperationPayment**](OperationPayment.md) |  | [optional] 
**participant** | [**\AllegroApiSDK\Model\SellerParticipant**](SellerParticipant.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


