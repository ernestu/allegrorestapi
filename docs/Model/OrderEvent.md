# # OrderEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | event id | 
**order** | [**\AllegroApiSDK\Model\OrderEventData**](OrderEventData.md) |  | 
**type** | [**\AllegroApiSDK\Model\CheckoutFormStatus**](CheckoutFormStatus.md) |  | 
**occurred_at** | [**\DateTime**](\DateTime.md) | Date when the event occurred | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


