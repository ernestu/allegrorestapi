# # OrderEventData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seller** | [**\AllegroApiSDK\Model\SellerReference**](SellerReference.md) |  | 
**buyer** | [**\AllegroApiSDK\Model\BuyerReference**](BuyerReference.md) |  | 
**line_items** | [**\AllegroApiSDK\Model\OrderLineItem[]**](OrderLineItem.md) |  | 
**checkout_form** | [**\AllegroApiSDK\Model\CheckoutFormReference**](CheckoutFormReference.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


