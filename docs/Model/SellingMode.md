# # SellingMode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**format** | [**\AllegroApiSDK\Model\SellingModeFormat**](SellingModeFormat.md) |  | [optional] 
**price** | [**\AllegroApiSDK\Model\BuyNowPrice**](BuyNowPrice.md) |  | [optional] 
**minimal_price** | [**\AllegroApiSDK\Model\MinimalPrice**](MinimalPrice.md) |  | [optional] 
**starting_price** | [**\AllegroApiSDK\Model\StartingPrice**](StartingPrice.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


