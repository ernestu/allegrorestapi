# # ListingOffer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | The offer ID. | [optional] 
**name** | **string** | The title of the offer. | [optional] 
**seller** | [**\AllegroApiSDK\Model\OfferSeller**](OfferSeller.md) |  | [optional] 
**promotion** | [**\AllegroApiSDK\Model\OfferPromotion**](OfferPromotion.md) |  | [optional] 
**delivery** | [**\AllegroApiSDK\Model\OfferDelivery**](OfferDelivery.md) |  | [optional] 
**images** | [**\AllegroApiSDK\Model\OfferImages[]**](OfferImages.md) | The gallery of images. Only the URL of the original sized image is provided. The first image represents the thumbnail image used on listing. | [optional] 
**selling_mode** | [**\AllegroApiSDK\Model\OfferSellingMode**](OfferSellingMode.md) |  | [optional] 
**stock** | [**\AllegroApiSDK\Model\OfferStock**](OfferStock.md) |  | [optional] 
**vendor** | [**\AllegroApiSDK\Model\OfferVendor**](OfferVendor.md) |  | [optional] 
**category** | [**\AllegroApiSDK\Model\OfferCategory**](OfferCategory.md) |  | [optional] 
**publication** | [**\AllegroApiSDK\Model\OfferPublication**](OfferPublication.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


