# # OfferAttachment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**type** | [**\AllegroApiSDK\Model\AttachmentType**](AttachmentType.md) |  | [optional] 
**file** | [**\AllegroApiSDK\Model\AttachmentFile**](AttachmentFile.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


