# # PromotionCampaignsResponseDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**promotion_campaigns** | [**\AllegroApiSDK\Model\SinglePromotionCampaignResponseDto[]**](SinglePromotionCampaignResponseDto.md) |  | 
**total_count** | **int** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


