# # SaleProductDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Product id. | 
**name** | **string** | Product name. | 
**category** | [**\AllegroApiSDK\Model\Category**](Category.md) |  | 
**eans** | **string[]** | A list of codes that identify this product. Currently this can include EAN, ISBN, and UPC identifier types. | [optional] 
**images** | [**\AllegroApiSDK\Model\ImageUrl[]**](ImageUrl.md) | List of product images. | [optional] 
**parameters** | [**\AllegroApiSDK\Model\ProductParameter[]**](ProductParameter.md) | List of product parameters. | [optional] 
**offer_requirements** | [**\AllegroApiSDK\Model\OfferRequirements**](OfferRequirements.md) |  | [optional] 
**compatibility_list** | [**\AllegroApiSDK\Model\SaleProductCompatibilityList**](SaleProductCompatibilityList.md) |  | [optional] 
**tecdoc_specification** | [**\AllegroApiSDK\Model\TecdocSpecification**](TecdocSpecification.md) |  | [optional] 
**description** | [**\AllegroApiSDK\Model\StandardizedDescription**](StandardizedDescription.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


