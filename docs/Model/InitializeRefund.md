# # InitializeRefund

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment** | [**\AllegroApiSDK\Model\RefundPayment**](RefundPayment.md) |  | 
**reason** | **string** | Reason for a payment refund. | 
**line_items** | [**\AllegroApiSDK\Model\RefundLineItem[]**](RefundLineItem.md) | List of order&#39;s line items which can be refunded. | [optional] 
**delivery** | [**\AllegroApiSDK\Model\InitializeRefundDelivery**](InitializeRefundDelivery.md) |  | [optional] 
**overpaid** | [**\AllegroApiSDK\Model\InitializeRefundOverpaid**](InitializeRefundOverpaid.md) |  | [optional] 
**surcharges** | [**\AllegroApiSDK\Model\PaymentsSurcharge[]**](PaymentsSurcharge.md) | List of surcharges for payment which can be refunded. | [optional] 
**additional_services** | [**\AllegroApiSDK\Model\InitializeRefundAdditionalServices**](InitializeRefundAdditionalServices.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


