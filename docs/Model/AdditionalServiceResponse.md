# # AdditionalServiceResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**configurations** | [**\AllegroApiSDK\Model\Configuration[]**](Configuration.md) |  | [optional] 
**definition** | [**\AllegroApiSDK\Model\BasicDefinitionResponse**](BasicDefinitionResponse.md) |  | [optional] 
**description** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


