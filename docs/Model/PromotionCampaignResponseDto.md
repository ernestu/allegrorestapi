# # PromotionCampaignResponseDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaign** | [**\AllegroApiSDK\Model\CampaignResponseDto**](CampaignResponseDto.md) |  | 
**link** | **string** |  | 
**promotion** | [**\AllegroApiSDK\Model\PromotionResponseDto**](PromotionResponseDto.md) |  | 
**status** | **string** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


