# # MultiPackBenefitSpecificationAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional] [default to 'UNIT_PERCENTAGE_DISCOUNT']
**configuration** | [**\AllegroApiSDK\Model\MultiPackBenefitSpecificationAllOfConfiguration**](MultiPackBenefitSpecificationAllOfConfiguration.md) |  | 
**trigger** | [**\AllegroApiSDK\Model\MultiPackBenefitSpecificationAllOfTrigger**](MultiPackBenefitSpecificationAllOfTrigger.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


