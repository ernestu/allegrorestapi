# # DictionaryCategoryParameter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional] [default to 'dictionary']
**restrictions** | [**\AllegroApiSDK\Model\DictionaryCategoryProductParameterAllOfRestrictions**](DictionaryCategoryProductParameterAllOfRestrictions.md) |  | [optional] 
**dictionary** | [**\AllegroApiSDK\Model\DictionaryCategoryProductParameterAllOfDictionary[]**](DictionaryCategoryProductParameterAllOfDictionary.md) | Defines the values accepted for this parameter. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


