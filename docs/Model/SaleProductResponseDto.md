# # SaleProductResponseDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | 
**name** | **string** | Name of the product. | 
**category** | [**\AllegroApiSDK\Model\Category**](Category.md) |  | 
**eans** | **string[]** | A list of codes that identify this product. Currently this can include EAN, ISBN, and UPC identifier types. | [optional] 
**images** | [**\AllegroApiSDK\Model\ImageUrl[]**](ImageUrl.md) |  | [optional] 
**parameters** | [**\AllegroApiSDK\Model\ProductParameter[]**](ProductParameter.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


