# # BadgeApplication

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Badge application ID. | 
**created_at** | **string** | Provided in [ISO 8601 format](link: https://en.wikipedia.org/wiki/ISO_8601). | 
**updated_at** | **string** | Provided in [ISO 8601 format](link: https://en.wikipedia.org/wiki/ISO_8601). | 
**campaign** | [**\AllegroApiSDK\Model\BadgeApplicationCampaign**](BadgeApplicationCampaign.md) |  | 
**offer** | [**\AllegroApiSDK\Model\BadgeApplicationOffer**](BadgeApplicationOffer.md) |  | 
**prices** | [**\AllegroApiSDK\Model\BadgeApplicationPrices**](BadgeApplicationPrices.md) |  | [optional] 
**process** | [**\AllegroApiSDK\Model\BadgeApplicationProcess**](BadgeApplicationProcess.md) |  | 
**purchase_constraints** | [**\AllegroApiSDK\Model\BadgeApplicationPurchaseConstraints**](BadgeApplicationPurchaseConstraints.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


