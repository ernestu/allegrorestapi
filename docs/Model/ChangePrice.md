# # ChangePrice

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | The unique command id provided in the input. | [optional] 
**input** | [**\AllegroApiSDK\Model\ChangePriceInput**](ChangePriceInput.md) |  | 
**output** | [**\AllegroApiSDK\Model\CommandOutput**](CommandOutput.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


