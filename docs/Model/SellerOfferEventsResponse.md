# # SellerOfferEventsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offer_events** | [**\AllegroApiSDK\Model\SellerOfferBaseEvent[]**](SellerOfferBaseEvent.md) | The list of events. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


