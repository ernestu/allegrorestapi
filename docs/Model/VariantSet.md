# # VariantSet

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offers** | [**\AllegroApiSDK\Model\VariantSetOffer[]**](VariantSetOffer.md) |  | 
**name** | **string** |  | 
**parameters** | [**\AllegroApiSDK\Model\VariantSetParameter[]**](VariantSetParameter.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


