# # ClassifiedResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**base_package** | [**\AllegroApiSDK\Model\ClassifiedPackage**](ClassifiedPackage.md) |  | 
**extra_packages** | [**\AllegroApiSDK\Model\ClassifiedPackage[]**](ClassifiedPackage.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


