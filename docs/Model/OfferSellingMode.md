# # OfferSellingMode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**format** | [**\AllegroApiSDK\Model\SellingModeFormat**](SellingModeFormat.md) |  | [optional] 
**price** | [**\AllegroApiSDK\Model\OfferPrice**](OfferPrice.md) |  | [optional] 
**fixed_price** | [**\AllegroApiSDK\Model\OfferFixedPrice**](OfferFixedPrice.md) |  | [optional] 
**popularity** | **int** | Popularity of the offer for *BUY_NOW* selling format. | [optional] 
**bid_count** | **int** | Number of bidders for *AUCTION* selling format. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


