# # CheckoutFormDeliveryReference

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**\AllegroApiSDK\Model\CheckoutFormDeliveryAddress**](CheckoutFormDeliveryAddress.md) |  | [optional] 
**method** | [**\AllegroApiSDK\Model\CheckoutFormDeliveryMethod**](CheckoutFormDeliveryMethod.md) |  | [optional] 
**pickup_point** | [**\AllegroApiSDK\Model\CheckoutFormDeliveryPickupPoint**](CheckoutFormDeliveryPickupPoint.md) |  | [optional] 
**cost** | [**\AllegroApiSDK\Model\Price**](Price.md) |  | [optional] 
**time** | [**\AllegroApiSDK\Model\CheckoutFormDeliveryTime**](CheckoutFormDeliveryTime.md) |  | [optional] 
**smart** | **bool** | Buyer used a SMART option | [optional] 
**calculated_number_of_packages** | **int** | Calculated number of packages. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


