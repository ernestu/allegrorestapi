# # ListingResponseOffers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**promoted** | [**\AllegroApiSDK\Model\ListingOffer[]**](ListingOffer.md) | List of promoted offers. | [optional] 
**regular** | [**\AllegroApiSDK\Model\ListingOffer[]**](ListingOffer.md) | List of regular (non-promoted) offers. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


