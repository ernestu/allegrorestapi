# # BadgeCampaign

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Badge campaign ID. | 
**name** | **string** | Badge campaign name. | 
**type** | **string** |  | 
**eligibility** | [**\AllegroApiSDK\Model\UserCampaignEligibility**](UserCampaignEligibility.md) |  | 
**application** | [**\AllegroApiSDK\Model\ApplicationTimePolicy**](ApplicationTimePolicy.md) |  | 
**visibility** | [**\AllegroApiSDK\Model\VisibilityTimePolicy**](VisibilityTimePolicy.md) |  | 
**publication** | [**\AllegroApiSDK\Model\PublicationTimePolicy**](PublicationTimePolicy.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


