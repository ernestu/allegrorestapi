<?php
/**
 * CheckoutForm
 *
 * PHP version 5
 *
 * @category Class
 * @package  AllegroApiSDK
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Allegro REST API
 *
 * https://developer.allegro.pl/about
 *
 * The version of the OpenAPI document: latest
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.2.3-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace AllegroApiSDK\Model;

use \ArrayAccess;
use \AllegroApiSDK\ObjectSerializer;

/**
 * CheckoutForm Class Doc Comment
 *
 * @category Class
 * @package  AllegroApiSDK
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class CheckoutForm implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'CheckoutForm';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'id' => 'string',
        'message_to_seller' => 'string',
        'buyer' => '\AllegroApiSDK\Model\CheckoutFormBuyerReference',
        'payment' => '\AllegroApiSDK\Model\CheckoutFormPaymentReference',
        'status' => '\AllegroApiSDK\Model\CheckoutFormStatus',
        'delivery' => '\AllegroApiSDK\Model\CheckoutFormDeliveryReference',
        'invoice' => '\AllegroApiSDK\Model\CheckoutFormInvoiceInfo',
        'line_items' => '\AllegroApiSDK\Model\CheckoutFormLineItem[]',
        'surcharges' => '\AllegroApiSDK\Model\CheckoutFormPaymentReference[]',
        'discounts' => '\AllegroApiSDK\Model\CheckoutFormDiscount[]',
        'summary' => '\AllegroApiSDK\Model\CheckoutFormSummary',
        'updated_at' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPIFormats = [
        'id' => 'uuid',
        'message_to_seller' => null,
        'buyer' => null,
        'payment' => null,
        'status' => null,
        'delivery' => null,
        'invoice' => null,
        'line_items' => null,
        'surcharges' => null,
        'discounts' => null,
        'summary' => null,
        'updated_at' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'message_to_seller' => 'messageToSeller',
        'buyer' => 'buyer',
        'payment' => 'payment',
        'status' => 'status',
        'delivery' => 'delivery',
        'invoice' => 'invoice',
        'line_items' => 'lineItems',
        'surcharges' => 'surcharges',
        'discounts' => 'discounts',
        'summary' => 'summary',
        'updated_at' => 'updatedAt'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'message_to_seller' => 'setMessageToSeller',
        'buyer' => 'setBuyer',
        'payment' => 'setPayment',
        'status' => 'setStatus',
        'delivery' => 'setDelivery',
        'invoice' => 'setInvoice',
        'line_items' => 'setLineItems',
        'surcharges' => 'setSurcharges',
        'discounts' => 'setDiscounts',
        'summary' => 'setSummary',
        'updated_at' => 'setUpdatedAt'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'message_to_seller' => 'getMessageToSeller',
        'buyer' => 'getBuyer',
        'payment' => 'getPayment',
        'status' => 'getStatus',
        'delivery' => 'getDelivery',
        'invoice' => 'getInvoice',
        'line_items' => 'getLineItems',
        'surcharges' => 'getSurcharges',
        'discounts' => 'getDiscounts',
        'summary' => 'getSummary',
        'updated_at' => 'getUpdatedAt'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['message_to_seller'] = isset($data['message_to_seller']) ? $data['message_to_seller'] : null;
        $this->container['buyer'] = isset($data['buyer']) ? $data['buyer'] : null;
        $this->container['payment'] = isset($data['payment']) ? $data['payment'] : null;
        $this->container['status'] = isset($data['status']) ? $data['status'] : null;
        $this->container['delivery'] = isset($data['delivery']) ? $data['delivery'] : null;
        $this->container['invoice'] = isset($data['invoice']) ? $data['invoice'] : null;
        $this->container['line_items'] = isset($data['line_items']) ? $data['line_items'] : null;
        $this->container['surcharges'] = isset($data['surcharges']) ? $data['surcharges'] : null;
        $this->container['discounts'] = isset($data['discounts']) ? $data['discounts'] : null;
        $this->container['summary'] = isset($data['summary']) ? $data['summary'] : null;
        $this->container['updated_at'] = isset($data['updated_at']) ? $data['updated_at'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['id'] === null) {
            $invalidProperties[] = "'id' can't be null";
        }
        if ($this->container['buyer'] === null) {
            $invalidProperties[] = "'buyer' can't be null";
        }
        if ($this->container['status'] === null) {
            $invalidProperties[] = "'status' can't be null";
        }
        if ($this->container['line_items'] === null) {
            $invalidProperties[] = "'line_items' can't be null";
        }
        if ($this->container['surcharges'] === null) {
            $invalidProperties[] = "'surcharges' can't be null";
        }
        if ($this->container['discounts'] === null) {
            $invalidProperties[] = "'discounts' can't be null";
        }
        if ($this->container['summary'] === null) {
            $invalidProperties[] = "'summary' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return string
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param string $id Checkout form id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets message_to_seller
     *
     * @return string|null
     */
    public function getMessageToSeller()
    {
        return $this->container['message_to_seller'];
    }

    /**
     * Sets message_to_seller
     *
     * @param string|null $message_to_seller Message from buyer to seller
     *
     * @return $this
     */
    public function setMessageToSeller($message_to_seller)
    {
        $this->container['message_to_seller'] = $message_to_seller;

        return $this;
    }

    /**
     * Gets buyer
     *
     * @return \AllegroApiSDK\Model\CheckoutFormBuyerReference
     */
    public function getBuyer()
    {
        return $this->container['buyer'];
    }

    /**
     * Sets buyer
     *
     * @param \AllegroApiSDK\Model\CheckoutFormBuyerReference $buyer buyer
     *
     * @return $this
     */
    public function setBuyer($buyer)
    {
        $this->container['buyer'] = $buyer;

        return $this;
    }

    /**
     * Gets payment
     *
     * @return \AllegroApiSDK\Model\CheckoutFormPaymentReference|null
     */
    public function getPayment()
    {
        return $this->container['payment'];
    }

    /**
     * Sets payment
     *
     * @param \AllegroApiSDK\Model\CheckoutFormPaymentReference|null $payment payment
     *
     * @return $this
     */
    public function setPayment($payment)
    {
        $this->container['payment'] = $payment;

        return $this;
    }

    /**
     * Gets status
     *
     * @return \AllegroApiSDK\Model\CheckoutFormStatus
     */
    public function getStatus()
    {
        return $this->container['status'];
    }

    /**
     * Sets status
     *
     * @param \AllegroApiSDK\Model\CheckoutFormStatus $status status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * Gets delivery
     *
     * @return \AllegroApiSDK\Model\CheckoutFormDeliveryReference|null
     */
    public function getDelivery()
    {
        return $this->container['delivery'];
    }

    /**
     * Sets delivery
     *
     * @param \AllegroApiSDK\Model\CheckoutFormDeliveryReference|null $delivery delivery
     *
     * @return $this
     */
    public function setDelivery($delivery)
    {
        $this->container['delivery'] = $delivery;

        return $this;
    }

    /**
     * Gets invoice
     *
     * @return \AllegroApiSDK\Model\CheckoutFormInvoiceInfo|null
     */
    public function getInvoice()
    {
        return $this->container['invoice'];
    }

    /**
     * Sets invoice
     *
     * @param \AllegroApiSDK\Model\CheckoutFormInvoiceInfo|null $invoice invoice
     *
     * @return $this
     */
    public function setInvoice($invoice)
    {
        $this->container['invoice'] = $invoice;

        return $this;
    }

    /**
     * Gets line_items
     *
     * @return \AllegroApiSDK\Model\CheckoutFormLineItem[]
     */
    public function getLineItems()
    {
        return $this->container['line_items'];
    }

    /**
     * Sets line_items
     *
     * @param \AllegroApiSDK\Model\CheckoutFormLineItem[] $line_items line_items
     *
     * @return $this
     */
    public function setLineItems($line_items)
    {
        $this->container['line_items'] = $line_items;

        return $this;
    }

    /**
     * Gets surcharges
     *
     * @return \AllegroApiSDK\Model\CheckoutFormPaymentReference[]
     */
    public function getSurcharges()
    {
        return $this->container['surcharges'];
    }

    /**
     * Sets surcharges
     *
     * @param \AllegroApiSDK\Model\CheckoutFormPaymentReference[] $surcharges surcharges
     *
     * @return $this
     */
    public function setSurcharges($surcharges)
    {
        $this->container['surcharges'] = $surcharges;

        return $this;
    }

    /**
     * Gets discounts
     *
     * @return \AllegroApiSDK\Model\CheckoutFormDiscount[]
     */
    public function getDiscounts()
    {
        return $this->container['discounts'];
    }

    /**
     * Sets discounts
     *
     * @param \AllegroApiSDK\Model\CheckoutFormDiscount[] $discounts discounts
     *
     * @return $this
     */
    public function setDiscounts($discounts)
    {
        $this->container['discounts'] = $discounts;

        return $this;
    }

    /**
     * Gets summary
     *
     * @return \AllegroApiSDK\Model\CheckoutFormSummary
     */
    public function getSummary()
    {
        return $this->container['summary'];
    }

    /**
     * Sets summary
     *
     * @param \AllegroApiSDK\Model\CheckoutFormSummary $summary summary
     *
     * @return $this
     */
    public function setSummary($summary)
    {
        $this->container['summary'] = $summary;

        return $this;
    }

    /**
     * Gets updated_at
     *
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->container['updated_at'];
    }

    /**
     * Sets updated_at
     *
     * @param string|null $updated_at Provided in [ISO 8601 format](link: https://en.wikipedia.org/wiki/ISO_8601).
     *
     * @return $this
     */
    public function setUpdatedAt($updated_at)
    {
        $this->container['updated_at'] = $updated_at;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


